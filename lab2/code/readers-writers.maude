fmod PETRI-NET is
	pr QID .
	
	sorts Place Marking Configuration .
	
	subsort Qid < Place .
	subsort Place < Marking .
	
	op nil : -> Marking [ctor] .
	op __ : Marking Marking -> Marking [ctor comm assoc id: nil] . 
	
	op <_> : Marking -> Configuration .
	
	op inhibits : Marking Marking -> Bool .
	
	vars P : Place .
	vars M M1 : Marking .
	
	eq inhibits( P M , P M1) = true .
	eq inhibits ( M1 , M) = false [owise] .
endfm

mod READERS-WRITERS is
	pr PETRI-NET .
	
	var M : Marking .
	
	rl [t1] : < 'p1 M > => < 'p2 M > .
	rl [t2] : < 'p2 M > => < 'p3 M > .
	rl [t3] : < 'p2 M > => < 'p4 M > .
	rl [t4] : < 'p3 'p5 M > => < 'p5 'p6 M > .
	crl [t5] : < 'p4 'p5 M > => < 'p7  M > if not(inhibits('p6, M)) .
	rl [t6] : < 'p6 M > => < 'p1 M > .
	rl [t7] : < 'p7 M > => < 'p1 'p5 M > .
endm

mod READERS-WRITERS-CHECK is
    pr READERS-WRITERS .
    pr INT .
    pr SATISFACTION .   *** modules for modelchecking
    pr MODEL-CHECKER .  *** modules for modelchecking
    pr LTL-SIMPLIFIER . *** modules for modelchecking
    
    *** A Configuration becomes a state for the Kripke Model
    subsort Configuration < State .
    
    var P : Place .
    var M : Marking .
    var N : Nat .
    
    *** A utility function to count tokens
    op count : Place Marking -> Int .
    eq count ( P, P  M ) = 1 + count(P, M) .
    eq count ( P, M ) = 0 [owise] .
    
    *** Defining two propositional "symbols"
    op ntoken : Place Int -> Prop .
    op sometoken : Place -> Prop .
    
    *** Semantics of propositional symbols
    ceq < M > |= ntoken(P,N) = true if (count(P,M) == N) .
    ceq < M > |= sometoken(P) = true if (count(P,M) > 0) . 
endm
	
*** reduce modelCheck ( < 'p1 'p5 > , <> sometoken('p2) ). // è vero che in tutti i percorsi finally ci sarà qualche token in p2?
*** reduce modelCheck ( < 'p1 'p5 > , <> sometoken('p7) ). // c'è almeno un percorso (quello in alto) in cui questa formula non è valida, maude da anche un controesempio
*** esiste un percorso in cui non capita mai di andare in p7 => devo chiedere la negata che è questa, cioè è vero che in tutti i percorsi nn si va mai in p7?
*** reduce modelCheck( < 'p1 'p5 >, [] ~ ntoken('p7,2)) . // globalmente non capiterà mai di avere due scrittori (mutua esclusione sugli scrittori)
*** reduce modelCheck ( < 'p1 'p5 > , [] <> ntoken('p1,1)). // ovunque il token si trovi riesce sempre a tornare in p1
*** reduce modelCheck ( < 'p1 'p1 'p5 > , [] <> ntoken('p1,2)).
*** reduce modelCheck ( < 'p1 'p1 'p1 'p1 'p1 'p5 > , [] <> ntoken('p1,2)).

*** query esistenziale la devo dare al negato!

*** es 1: reduce modelCheck ( < 'p1 'p1 'p1 'p5 >, [] ~ ntoken('p6, 4)) .
*** es 2: reduce modelCheck ( < 'p1 'p1 'p1 'p5 >, ~ <> ntoken('p6,3)) .
*** es 2: reduce modelCheck ( < 'p1 'p1 'p1 'p5 >, [] ~ ntoken('p6,3)) .

*** esiste un percorso in cui contemporaneamente ho p6 e p7?
*** reduce modelCheck ( < 'p1 'p1 'p1 'p5 >, [] ~ (sometoken('p6) /\ sometoken('p7))) .
