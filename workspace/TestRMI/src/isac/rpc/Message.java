package isac.rpc;

import java.io.Serializable;

public class Message implements Serializable{

	private String body;

	public Message(String body){
		this.body = body;
	}
	
	public String getContent(){
		return this.body;
	}
	
}
