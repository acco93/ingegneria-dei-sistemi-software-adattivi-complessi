package isac.rpc;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
        
public class ServerExt  {
                
    public static void main(String args[]) {
        
        try {
        	HelloServiceExtended obj = new HelloServiceExtendedImpl();
            HelloServiceExtended stub = (HelloServiceExtended) UnicastRemoteObject.exportObject(obj, 0);
            
            // Bind the remote object's stub in the registry
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind("HelloExtended", stub);
            
            
            System.out.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}