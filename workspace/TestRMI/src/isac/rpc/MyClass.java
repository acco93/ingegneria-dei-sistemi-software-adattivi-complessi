package isac.rpc;

public class MyClass implements java.io.Serializable, MyClassInt  {

	private int x;
	
	public MyClass(int x){
		this.x = x;
	}
	
	public int get(){
		return x;
	}
	
	public void update(int c){
		x = c;
	}
}
