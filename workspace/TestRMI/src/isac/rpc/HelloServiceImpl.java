package isac.rpc;

import java.rmi.RemoteException;

public class HelloServiceImpl implements HelloService {
        
    public HelloServiceImpl() {}

    public String sayHello() {
        return "Hello, world!";
    }
    
    public String sayHello(int n) {
        return "Hello, world! ==> " + n;
    }

    public String sayHello(MyClassInt obj) throws RemoteException {
    	/*
    	 * la modifica avviene effettivamente
    	 * */
    	obj.update(obj.get()+1);
        return "Hello, world! ==> " + obj.get();
    }
        
}