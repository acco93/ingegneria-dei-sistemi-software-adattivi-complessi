package isac.rpc;

import java.rmi.RemoteException;

public class CounterImpl implements Counter {

	private int n;

	public CounterImpl(int init) {
		this.n = init;
	}

	@Override
	public void inc() {
		this.n++;
	}

	@Override
	public int getValue() throws RemoteException {
		return n;
	}

}
