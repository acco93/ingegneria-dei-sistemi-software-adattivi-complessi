package isac.rpc;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface HelloService extends Remote {
	
    String sayHello() throws RemoteException;
    
    String sayHello(int n) throws RemoteException;

    String sayHello(MyClassInt obj) throws RemoteException;

    /*
     * Non posso modificare l'interfaccia perchè corromperei il sistema,
     * potrei definire una interfaccia che estende questa...
     */
    
}