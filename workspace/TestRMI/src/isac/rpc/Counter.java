package isac.rpc;

import java.rmi.Remote;
import java.rmi.RemoteException;

interface Counter extends Remote {
	void inc() throws RemoteException;
	int getValue() throws RemoteException;
}
