package isac.rpc;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Client {

    private Client() {}

    public static void main(String[] args) {

        String host = (args.length < 1) ? null : args[0];
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            //HelloService stub = (HelloService) registry.lookup("Hello");
            
            Counter c = (Counter) registry.lookup("Counter");
            
            HelloServiceExtended stub = (HelloServiceExtended) registry.lookup("HelloExtended");
                 
            String response = stub.sayHello();
            System.out.println("response: " + response);
            /*
             * Marshaling dei parametri, gli obj devono essere serializzabili per poterli
             * marshalllare!! I tipi primitivi lo sono già
             */
            System.out.println("response: " + stub.sayHello(10));
            
            stub.sayHello(new Message("Ciao"));
            
            
            System.out.println("Counter value: "+c.getValue());
            c.inc();
            System.out.println("Counter value after inc: "+c.getValue());
            
            /*
            String response = stub.sayHello(obj);
            System.out.println("response: " + response);
            System.out.println(">> "+obj.get());
            */
            
            /*
             * Passando un oggetto remoto lui capisce che vogliamo interagire con
             * l'oggetto stesso e non una copia come invece farebbe senza Remote
             * */
            MyClass obj = new MyClass(300); 
            /*
             * Lo devo esportare nel senso che è quello sul server, non una copia
             */
            UnicastRemoteObject.exportObject(obj, 0);
            
            response = stub.sayHello(obj);
            System.out.println(response);
            System.out.println(obj.get());
            
            
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}