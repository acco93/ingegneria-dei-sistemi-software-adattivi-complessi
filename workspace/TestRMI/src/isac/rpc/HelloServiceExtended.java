package isac.rpc;

import java.rmi.RemoteException;

public interface HelloServiceExtended extends HelloService{

	void sayHello(Message msg) throws RemoteException;
	
}
