package isac.rpc;

import java.rmi.*;

public interface MyClassInt extends Remote {

	int get() throws RemoteException;

	void update(int c) throws RemoteException;
	
}