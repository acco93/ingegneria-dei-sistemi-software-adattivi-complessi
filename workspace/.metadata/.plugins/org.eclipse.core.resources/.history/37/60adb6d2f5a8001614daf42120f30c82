package contractnetprotocol.mom;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Contractor {

	private String name;
	private Skills skills;

	public Contractor(String name) {
		this.name = name;
		this.skills = new Skills();
		log("started ...");

		Gson gson = new Gson();

		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			Connection connection;

			connection = factory.newConnection();
			Channel channel = connection.createChannel();

			channel.exchangeDeclare(R.EXCHANGE_NAME, "fanout");
			String queueName = channel.queueDeclare().getQueue();
			channel.queueBind(queueName, R.EXCHANGE_NAME, "");
			
			Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					/*
					 * Wait for the task announcement
					 */
					Task task = gson.fromJson(message, Task.class);
					log("received " + task.toString());
					/*
					 * Evaluate the request
					 */
					
					Optional<ProcessingInfo> processingInfo = taskEvaluation(task);
					
					if (!processingInfo.isPresent()) {
						log("ops ... not able to perform the task ... ");
						return;
					} else {
						log("able to perform the task!");			
						log(processingInfo.get().toString());
						
						/*
						 * Define a bid
						 */
						
						Bid bid = new Bid(new Random().nextInt(100));
						
						/*
						 * Send the bid to the manager
						 */
						Channel bidChannel = connection.createChannel();
						bidChannel.queueDeclare(R.BIDS_QUEUE, false, false, false, null);
						String jsonBid = gson.toJson(bid);
						bidChannel.basicPublish("", R.BIDS_QUEUE, null, jsonBid.getBytes());
						try {
							bidChannel.close();
						} catch (TimeoutException e) {
							e.printStackTrace();
						}
						
						/*
						 * Compute
						 */
						int methodIndex = processingInfo.get().getMethodIndex();
						Object[] params = processingInfo.get().getParsedParams();
						try {
							Object resultObject = Skills.class.getMethods()[methodIndex].invoke(skills, params);
							
							String result = resultObject.toString();
							
							/*
							 * Send the reply
							 */
							
							new Result(task.getName(), result);

						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
								| SecurityException e) {
							e.printStackTrace();
						}
					}
						

				}

			};
			channel.basicConsume(queueName, true, consumer);

		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}

	}

	private Optional<ProcessingInfo> taskEvaluation(Task task) {
		
		SupportedTypes[] taskParamsTypes = new SupportedTypes[task.getParams().length];
		Object[] parsedParams = new Object[task.getParams().length];
		
		/*
		 * Check if exists a method with the given name
		 */

		for (int i=0;i<Skills.class.getMethods().length;i++) {

			Method method = Skills.class.getMethods()[i];
			
			boolean error = false;

			if (method.getName().equals(task.getName())) {
				/*
				 * Ok! Now check the parameters! Since I've a string
				 * representation, try to cast it in the type required from the
				 * method
				 */
				if (method.getParameterCount() != task.getParams().length) {
					continue;
				}

				for (int j = 0; j < method.getParameterCount() && !error; j++) {

					String parType = method.getParameters()[j].getType().getTypeName();
					String param = task.getParams()[j];

					System.out.println(parType + " " +SupportedTypes.INT);
					
					if (parType.equals(SupportedTypes.INT.toString()) || parType.equals(SupportedTypes.INT_CLASS.toString())) {
						try {
							parsedParams[j] = Integer.parseInt(param);
							taskParamsTypes[j] = SupportedTypes.INT;
						} catch (NumberFormatException e) {
							error = true;
							continue;
						}
					} else if (parType.equals(SupportedTypes.FLOAT.toString()) || parType.equals(SupportedTypes.FLOAT_CLASS.toString())) {
						try {
							parsedParams[j] =  Float.parseFloat(param);
							taskParamsTypes[j] = SupportedTypes.FLOAT;
						} catch (NumberFormatException e) {
							error = true;
							continue;
						}
					} else if (parType.equals(SupportedTypes.STRING.toString())) {
						// strings are ok!
						parsedParams[j] = new String(param);
						taskParamsTypes[j] = SupportedTypes.STRING;
					} else {
						error = true;
						continue;
					}

				}

				if(!error){
					String returnTypeString = method.getReturnType().getName();
					
					SupportedTypes returnType = null;
					
					if(returnTypeString.equals(SupportedTypes.INT.toString()) || returnTypeString.equals(SupportedTypes.INT_CLASS.toString())){
						returnType = SupportedTypes.INT;
					} else if(returnTypeString.equals(SupportedTypes.FLOAT.toString()) || returnTypeString.equals(SupportedTypes.FLOAT_CLASS.toString())) {
						returnType = SupportedTypes.FLOAT;
					} else if(returnTypeString.equals(SupportedTypes.STRING.toString())) {
						returnType = SupportedTypes.STRING;
					}
					
					return Optional.of(new ProcessingInfo(i,taskParamsTypes,returnType, parsedParams));
				}
			}
		}

		return Optional.empty();

	}

	private void log(String text) {
		System.out.println("[" + this.name + "] " + text);
	}

}
