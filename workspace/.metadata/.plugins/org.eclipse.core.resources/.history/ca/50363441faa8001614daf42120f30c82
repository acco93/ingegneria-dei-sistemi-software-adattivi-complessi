package contractnetprotocol.mom;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Contractor {

	private String name;
	private Skills skills;
	private String privateChannelName;

	public Contractor(String name) {
		this.name = name;
		this.skills = new Skills();
		this.privateChannelName = UUID.randomUUID().toString();
		
		log("started ...");

		Gson gson = new Gson();

		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			Connection connection;

			connection = factory.newConnection();
			Channel channel = connection.createChannel();

			channel.exchangeDeclare(R.EXCHANGE_NAME, "fanout");
			String queueName = channel.queueDeclare().getQueue();
			channel.queueBind(queueName, R.EXCHANGE_NAME, "");
			
			Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					/*
					 * Wait for the task announcement
					 */
					Task task = gson.fromJson(message, Task.class);
					log("received " + task.toString());
					/*
					 * Evaluate the request
					 */
					
					Optional<ProcessingInfo> processingInfo = Utilities.taskEvaluation(task);
					
					if (!processingInfo.isPresent()) {
						log("ops ... not able to perform the task ... ");
						return;
					} else {
						log("able to perform the task!");			
						log(processingInfo.get().toString());
						
						/*
						 * Define a bid
						 */
						
						Bid bid = new Bid(name, new Random().nextInt(100), privateChannelName);
						
						/*
						 * Send the bid to the manager
						 */
						Channel bidChannel = connection.createChannel();
						bidChannel.queueDeclare(R.BIDS_QUEUE, false, false, false, null);
						String jsonBid = gson.toJson(bid);
						bidChannel.basicPublish("", R.BIDS_QUEUE, null, jsonBid.getBytes());
						try {
							bidChannel.close();
						} catch (TimeoutException e) {
							e.printStackTrace();
						}
						
						/*
						 * Wait for a reply in a private channel
						 */
						
						Channel replyChannel = connection.createChannel();
						replyChannel.queueDeclare(privateChannelName, false, false,false , null);
						
						replyChannel.basicConsume(privateChannelName, new DefaultConsumer(replyChannel){
							public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
									byte[] body) throws IOException {
						
							   Reply reply = gson.fromJson(new String(body, "UTF-8"),Reply.class);
							   
							   if(!reply.performWork){
									return;
								} else {
									
									/*
									 * Compute
									 */
									int methodIndex = processingInfo.get().getMethodIndex();
									Object[] params = processingInfo.get().getParsedParams();
									try {
										Object resultObject = Skills.class.getMethods()[methodIndex].invoke(skills, params);
										
										String result = resultObject.toString();
										
										/*
										 * Send the reply
										 */
										
										new Result(task.getName(), result);

									} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
											| SecurityException e) {
										e.printStackTrace();
									}
								}
							   
							   
							}
						});
								
					}
						

				}

			};
			channel.basicConsume(queueName, true, consumer);

		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}

	}



	private void log(String text) {
		System.out.println("[" + this.name + "] " + text);
	}

}
