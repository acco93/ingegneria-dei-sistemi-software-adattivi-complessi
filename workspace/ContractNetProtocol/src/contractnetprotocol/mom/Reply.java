package contractnetprotocol.mom;

public class Reply {
	
	private boolean performWork;
	private String queueName;
	
	public Reply (boolean performWork, String queueName){
		this.performWork = performWork;
		this.queueName = queueName;
	}

	public boolean isPerformWork() {
		return performWork;
	}

	public String getQueueName() {
		return queueName;
	}
	
	
	
}
