package contractnetprotocol.mom;

public enum SupportedTypes {
	INT, INT_CLASS, FLOAT, FLOAT_CLASS, STRING;
	
	@Override
	  public String toString() {
	    switch(this) {
	      case INT: return "int";
	      case INT_CLASS: return "java.lang.Integer";
	      
	      case FLOAT: return "float";
	      case FLOAT_CLASS: return "java.lang.Float";
	      
	      case STRING: return "java.lang.String";
	      
	      default: throw new IllegalArgumentException();
	    }
	  }
	
}
