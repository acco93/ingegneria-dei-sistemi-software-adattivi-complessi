package contractnetprotocol.mom;

import java.util.Arrays;

public class ProcessingInfo {
	private int methodIndex;
	private SupportedTypes[] paramsType;
	private Object[] parsedParams;
	private SupportedTypes returnType;

	public ProcessingInfo(int methodIndex, SupportedTypes[] paramsType, SupportedTypes returnType, Object[] parsedParams){
		this.methodIndex = methodIndex;
		this.paramsType = paramsType;
		this.returnType = returnType;
		this.parsedParams = parsedParams;
	}

	public int getMethodIndex() {
		return methodIndex;
	}

	public SupportedTypes[] getParamsType() {
		return paramsType;
	}

	public Object[] getParsedParams() {
		return parsedParams;
	}

	public SupportedTypes getReturnType() {
		return returnType;
	}

	@Override
	public String toString() {
		return "ProcessingInfo [methodIndex=" + methodIndex + ", paramsType=" + Arrays.toString(paramsType)
				+ ", parsedParams=" + Arrays.toString(parsedParams) + ", returnType=" + returnType + "]";
	}
	
}
