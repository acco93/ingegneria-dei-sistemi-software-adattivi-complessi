package contractnetprotocol.mom;

/*
 * A simple timer.
 */

public class Timer extends Thread {

	private volatile int time;

	public Timer(int time) {
		this.time = time;
	}
	
	public boolean timeout() {
		return time > 0 ? false : true;
	}

	public synchronized void run() {

		while (time > 0) {

			time--;

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

			}

		}
		
		this.notifyAll();
	}

	public synchronized void waitTimeout(){
		
		while(!this.timeout()){
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
