package contractnetprotocol.mom;

/*
 * Contractor bid.
 */
public class Bid {

	private String contractorName;
	private int value;
	private String replyChannelName;

	public Bid(String contractorName, int value, String replyChannelName) {
		this.contractorName = contractorName;
		this.value = value;
		this.replyChannelName = replyChannelName;
	}

	public String getContractorName(){
		return this.contractorName;
	}
	
	public int getValue() {
		return value;
	}

	public String getReplyChannelName() {
		return replyChannelName;
	}

	@Override
	public String toString() {
		return "Bid [contractorName=" + contractorName + ", value=" + value + ", replyChannelName=" + replyChannelName
				+ "]";
	}

	
	
}
