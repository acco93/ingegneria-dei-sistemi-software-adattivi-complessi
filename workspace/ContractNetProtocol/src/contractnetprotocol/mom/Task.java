package contractnetprotocol.mom;

import java.util.Arrays;

public class Task {
	
	private final String name;
	private final String description;
	private final String[] params;

	public Task(String name, String description, String ... params ){
		this.name = name;
		this.description = description;
		this.params = params;
	}

	@Override
	public String toString() {
		return "Task [name=" + name + ", description=" + description + ", params=" + Arrays.toString(params) + "]";
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String[] getParams() {
		return params;
	}
	
	
	
}
