package contractnetprotocol.mom;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Manager {

	private static final int WAIT_TIME = 10;
	Map<String, Bid> bids;

	public Manager() {

		log("started ...");

		bids = new HashMap<>();

		String[] params = new String[1];
		params[0] = new String("45");
		Task fibTask = new Task("fib", "Compute the n-th fibonacci number", params);

		params = new String[2];
		params[0] = new String("10");
		params[1] = new String("5");
		Task sumTask = new Task("sum", "Compute the sum between two numbers", params);

		params = new String[1];
		params[0] = "Hello";
		Task stringTask = new Task("duplicate","Duplicate the given string",params);
		
		Gson gson = new Gson();
		String json = gson.toJson(sumTask);
		log(json);

		/*
		 * Wait for contractors to join the channel
		 */

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		try {
			/*
			 * The manager sends the task announcement.
			 */

			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			/*
			 * The connection abstracts the socket connection, and takes care of
			 * protocol version negotiation and authentication and so on for us.
			 */
			Connection connection;
			connection = factory.newConnection();
			/*
			 * Next we create a channel, which is where most of the API for
			 * getting things done resides.
			 */
			Channel channel = connection.createChannel();

			channel.exchangeDeclare(R.EXCHANGE_NAME, "fanout");

			/*
			 * Send the task to everyone!
			 */
			channel.basicPublish(R.EXCHANGE_NAME, "", null, json.getBytes("UTF-8"));
			log("Task announced!");

			channel.close();

			/*
			 * Open a bids channel and register an handler.
			 */
			Channel bidsChannel = connection.createChannel();

			bidsChannel.queueDeclare(R.BIDS_QUEUE, false, false, false, null);

			Consumer consumer = new DefaultConsumer(bidsChannel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
						byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					Bid bid = gson.fromJson(message, Bid.class);
					log("Bid received: '" + bid + "'");
					bids.put(bid.getContractorName(), bid);
				}
			};
			
			bidsChannel.basicConsume(R.BIDS_QUEUE, true, consumer);

			/*
			 * Wait XXX second before deciding the winner
			 */
			
			log("waiting "+WAIT_TIME+" seconds for bids");
			Timer t = new Timer(WAIT_TIME);
			t.start();
			t.waitTimeout();
			
			log("evaluating the bids");
			
			Optional<Bid> bestBid = this.evaluateBids();
			
			if(bestBid.isPresent()){
				log("Best bid: "+bestBid.get());
				this.replyToEveryone(connection,bestBid.get());
			} else {
				log("no bids :(");
			}
			
			connection.close();
		} catch (IOException | TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void replyToEveryone(Connection connection, Bid bestBid) throws IOException, TimeoutException {
		
		/*
		 * Send replies to losers
		 */
		for(Entry<String,Bid> entry: bids.entrySet()){
			String contractorName = entry.getKey();
			
			if(!contractorName.equals(bestBid.getContractorName())){
				log("Sorry "+ contractorName+ ", you've lost ... ");
				/*
				 * Define the channel
				 */
				
				Bid bid = entry.getValue();
				String reply = new Gson().toJson(new Reply(false, null));
				
				
				Channel privateChannel = connection.createChannel();
				privateChannel.queueDeclare(bid.getReplyChannelName(), false, false, false, null);
				privateChannel.basicPublish("", bid.getReplyChannelName(), null, reply.getBytes());
				
				privateChannel.close();
			}
		}
		
		/*
		 * Send the reply to the winner
		 */
		String responseQueueName = UUID.randomUUID().toString();
		String reply = new Gson().toJson(new Reply(true, responseQueueName));
		
		log("Sending the reply to the winner ("+bestBid.getContractorName()+")... ");
		Channel winnerChannel = connection.createChannel();
		winnerChannel.queueDeclare(bestBid.getReplyChannelName(), false, false, false, null);
		winnerChannel.basicPublish("", bestBid.getReplyChannelName(), null, reply.getBytes());
		
		/*
		 * Registering an handler
		 */
		Channel resultChannel = connection.createChannel();
		resultChannel.queueDeclare(responseQueueName, false, false, false, null);
		log("Waiting for the result..");
		resultChannel.basicConsume(responseQueueName, new DefaultConsumer(resultChannel){
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				Result result = new Gson().fromJson(message, Result.class);
				log("Result: '" + result + "'");
			}
		});
		
		winnerChannel.close();
	}

	private Optional<Bid> evaluateBids() {
		Optional<Bid> bestBid = Optional.empty();
		for(Entry<String, Bid> entry : bids.entrySet()){
			if(!bestBid.isPresent() || entry.getValue().getValue() < bestBid.get().getValue()){
				bestBid = Optional.of(entry.getValue());
			}
		}
		return bestBid;
	}

	private void log(String text) {
		System.out.println("[Manager] " + text);
	}

}
