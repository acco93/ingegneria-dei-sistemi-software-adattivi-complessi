package contractnetprotocol.mom;

import java.lang.reflect.Method;
import java.util.Optional;

public class Utilities {

	public static Optional<ProcessingInfo> taskEvaluation(Task task) {
		
		SupportedTypes[] taskParamsTypes = new SupportedTypes[task.getParams().length];
		Object[] parsedParams = new Object[task.getParams().length];
		
		/*
		 * Check if exists a method with the given name
		 */

		for (int i=0;i<Skills.class.getMethods().length;i++) {

			Method method = Skills.class.getMethods()[i];
			
			boolean error = false;

			if (method.getName().equals(task.getName())) {
				/*
				 * Ok! Now check the parameters! Since I've a string
				 * representation, try to cast it in the type required from the
				 * method
				 */
				if (method.getParameterCount() != task.getParams().length) {
					continue;
				}

				for (int j = 0; j < method.getParameterCount() && !error; j++) {

					String parType = method.getParameters()[j].getType().getTypeName();
					String param = task.getParams()[j];
					
					if (parType.equals(SupportedTypes.INT.toString()) || parType.equals(SupportedTypes.INT_CLASS.toString())) {
						try {
							parsedParams[j] = Integer.parseInt(param);
							taskParamsTypes[j] = SupportedTypes.INT;
						} catch (NumberFormatException e) {
							error = true;
							continue;
						}
					} else if (parType.equals(SupportedTypes.FLOAT.toString()) || parType.equals(SupportedTypes.FLOAT_CLASS.toString())) {
						try {
							parsedParams[j] =  Float.parseFloat(param);
							taskParamsTypes[j] = SupportedTypes.FLOAT;
						} catch (NumberFormatException e) {
							error = true;
							continue;
						}
					} else if (parType.equals(SupportedTypes.STRING.toString())) {
						// strings are ok!
						parsedParams[j] = new String(param);
						taskParamsTypes[j] = SupportedTypes.STRING;
					} else {
						error = true;
						continue;
					}

				}

				if(!error){
					String returnTypeString = method.getReturnType().getName();
					
					SupportedTypes returnType = null;
					
					if(returnTypeString.equals(SupportedTypes.INT.toString()) || returnTypeString.equals(SupportedTypes.INT_CLASS.toString())){
						returnType = SupportedTypes.INT;
					} else if(returnTypeString.equals(SupportedTypes.FLOAT.toString()) || returnTypeString.equals(SupportedTypes.FLOAT_CLASS.toString())) {
						returnType = SupportedTypes.FLOAT;
					} else if(returnTypeString.equals(SupportedTypes.STRING.toString())) {
						returnType = SupportedTypes.STRING;
					}
					
					return Optional.of(new ProcessingInfo(i,taskParamsTypes,returnType, parsedParams));
				}
			}
		}

		return Optional.empty();

	}
	
}
