package contractnetprotocol.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ManagerInterface extends Remote {
	
	//Optional<Task> proposeBid(Bid bid) throws RemoteException;
	/**
	 * 
	 * Returns true if the bid has been successfully done.
	 * The manager accepts bids within a fixed time interval.
	 * @param name
	 * @param bid
	 * @return
	 * @throws RemoteException
	 */
	boolean proposeBid(String name, Bid bid) throws RemoteException;

	//void waitTimeout() throws RemoteException;
	
	//void selectBid() throws RemoteException;

	void sendResult(String name, Object value) throws RemoteException;

	
}
