package contractnetprotocol.rmi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

/*
 * Manager implementation.
 */
public class Manager implements ManagerInterface {

	public static final int MAX_BID_TIME = 10;
	private Map<String, Bid> contractors;
	private Timer timer;
	private AbstractTask<Integer> task;
	private Registry registry;
	private String winnerName;

	public Manager() {
		try {

			this.registry = LocateRegistry.getRegistry();
			
			ManagerInterface managerStub = (ManagerInterface) UnicastRemoteObject.exportObject(this, 0);
			registry.rebind(R.MANAGER, managerStub);
			
			log("started ...");
			this.task = new FibonacciTask(45);
			/*
			 * Save the set of contractors as pair <name, bid>
			 */
			this.contractors = new HashMap<String, Bid>();
			

			/*
			 * Wait for bids.
			 */
			this.waitTimeout();
			/*
			 * Select the winner
			 */
			this.selectBid();
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		;

	}

	public void waitTimeout() {
		this.timer = new Timer(MAX_BID_TIME);
		log("waiting " + MAX_BID_TIME + " seconds for bids...");
		this.timer.start();
		this.timer.waitTimeout();
		log("timeout occured");
	}

	public void selectBid() {

		Optional<String> contractorName = Optional.empty();
		Optional<Bid> bestBid = Optional.empty();

		for (Entry<String, Bid> entry : contractors.entrySet()) {
			if (bestBid.isPresent()) {
				if (bestBid.get().value() > entry.getValue().value()) {
					bestBid = Optional.of(entry.getValue());
					contractorName = Optional.of(entry.getKey());
				}
			} else {
				bestBid = Optional.of(entry.getValue());
				contractorName = Optional.of(entry.getKey());
			}
		}

		contractorName.ifPresent(name -> {
			try {
				this.winnerName = name;
				log("winner name: " + name);
				ContractorInterface contractor = (ContractorInterface) registry.lookup(name);
				contractor.reply(task);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NotBoundException e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public boolean proposeBid(String contractorName, Bid bid) throws RemoteException {

		if (timer.timeout()) {
			return false;
		}

		log("contractor " + contractorName + " proposes " + bid.value());
		this.contractors.put(contractorName, bid);
		return true;
	}

	@Override
	public void sendResult(String name, Object value) {
		if (name.equals(winnerName)) {
			log("Result received: " + value);
		} else {
			log("Invalid method invocation");
		}

	}

	private void log(String msg) {
		System.out.println("[Manager] " + msg);
	}

}
