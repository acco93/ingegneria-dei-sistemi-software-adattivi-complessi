package contractnetprotocol.rmi;

/*
 * Task implementation example
 */
public class FibonacciTask extends AbstractTask<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int n;

	public FibonacciTask(int n){
		this.n = n;
	}
	
	@Override
	public Integer execute() {
		return fib(n);
	}

	private Integer fib(int n) {
		
		if(n==0){ return 0; }
		if(n==1){ return 1; }
		
		return fib(n-1) + fib(n-2); 
	}

	@Override
	public String toString() {
		return "FibonacciTask [n=" + n + "]";
	}

	
	
}
