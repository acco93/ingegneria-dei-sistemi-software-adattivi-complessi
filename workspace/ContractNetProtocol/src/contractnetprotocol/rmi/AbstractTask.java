package contractnetprotocol.rmi;

import java.io.Serializable;
/*
 * Abstract class that represents a generic task.
 */
public abstract class AbstractTask<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract T execute();

}
