package contractnetprotocol.rmi;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ContractorInterface extends Remote  {
	
	<T> void reply(AbstractTask<T> task) throws RemoteException;
	
}
