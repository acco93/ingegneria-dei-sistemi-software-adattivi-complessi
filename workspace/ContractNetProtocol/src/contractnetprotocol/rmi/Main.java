package contractnetprotocol.rmi;

class Letters {
	
	private char start;

	public Letters(char start){
		this.start = start;
	}
	
	public char next(){
		return start++;
	}
}

public class Main {

	public static void main(String[] args) {

		new Thread(()->{new Manager();}).start();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Letters l = new Letters('A');
		
		for(int i=0;i<22;i++){
			new Thread(()->{
				new Contractor(String.valueOf(l.next()));
			}).start();			
		}

		
	}

	
	
}
