package contractnetprotocol.rmi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Optional;
import java.util.Random;

/*
 * Contractor implementation.
 */
public class Contractor implements ContractorInterface {

	private static final int MAX_WAIT_TIMEOUT = 5+Manager.MAX_BID_TIME;
	private String name;
	private Bid bid;
	private Timer timer;
	private Optional<AbstractTask> task;

	public Contractor(String name) {
		this.name = name;
		this.bid = new Bid(new Random().nextInt(100));
		this.task = Optional.empty();
		this.timer = new Timer(MAX_WAIT_TIMEOUT);

		String host = null;

		try {
			/*
			 * Retrieve the registry
			 */
			Registry registry = LocateRegistry.getRegistry(host);
			/*
			 * Retrieve the manager remote reference
			 */
			ManagerInterface manager = (ManagerInterface) registry.lookup(R.MANAGER);
			/*
			 * Export this object in order to allow the manager to contact it
			 */
			ContractorInterface contractorStub = (ContractorInterface) UnicastRemoteObject.exportObject(this, 0);
			/*
			 * Use the object name as reference
			 */
			registry.rebind(this.name, contractorStub);
			
			log("bidding ("+this.bid.value()+")");
			
			/*
			 * Propose a bid and check if it has been accepted by the manager,
			 * send the name so the manager is able to reply
			 */
			boolean result = manager.proposeBid(this.name, bid);

			if(result) {
				log("bid accepted");
			} else {
				log("bid refused, bye!");
				return;
			}

			log("waiting "+MAX_WAIT_TIMEOUT+" seconds..");
			
			/*
			 * Let the manager the time to gather other contractors bid.
			 */
			this.timer.start();
			this.timer.waitTimeout();
			
			/*
			 * Check if the manager has chosen this contractor as winner
			 */
			if(!task.isPresent()){
				log("I've lost...");
			} else {
				log("I'm the winner!");
				log("Executing "+task);
				/*
				 * Execute the task and send the result
				 */
				Object value = task.get().execute();
				manager.sendResult(this.name, value);
			}
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}

	}


	@Override
	public <T> void reply(AbstractTask<T> task) throws RemoteException {
		this.task = Optional.ofNullable(task);
	}

	private void log(String msg){
		System.out.println("[Contractor "+this.name+"] "+msg);
	}
	
}
