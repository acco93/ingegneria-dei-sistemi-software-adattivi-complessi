package contractnetprotocol.rmi;

import java.io.Serializable;
/*
 * Contractor bid.
 */
public class Bid implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int value;

	public Bid(int value){
		this.value = value;
	}
	
	public int value() {
		return this.value;
	}

}
